<?php

abstract class Crypto
{
    const CIPHER = 'aes-256-cbc';
    const HMAC_ALGO = 'sha256';

    /**
     * Number of bytes generated for encryption key
     * 64 = 512-bit
     * 32 = 256-bit
     * 16 = 128-bit
     */
    const CSPRNG_BYTES = 32;
    /**
     * Number of characters generated for HMAC (hex)
     */
    const HMAC_XBYTES = 64;

    /**
     * @param string $data - data to encrypt
     * @param string|null $encryptionKey - hex
     * @return string - encrypted data
     */
    final public static function encrypt($data, $encryptionKey = null)
    {
        if (null === $encryptionKey) {
            $encryptionKey = static::getEncryptionKey();
        }

        $iv = static::getInitializationVector();

        $encrypted = openssl_encrypt(
            $data,
            static::CIPHER,
            hex2bin($encryptionKey),
            OPENSSL_RAW_DATA,
            $iv
        );

        $encrypted .= static::getHMAC($encrypted, $encryptionKey); // add authentication code

        return sprintf('%s|%s', base64_encode($encrypted), base64_encode($iv));
    }

    /**
     * @param string $encrypted - encrypted data
     * @param string|null $encryptionKey - hex
     * @return bool|string - decrypted data or false on failure
     */
    final public static function decrypt($encrypted, $encryptionKey = null)
    {
        list($data, $iv) = explode('|', $encrypted);

        if (null === $encryptionKey) {
            $encryptionKey = static::getEncryptionKey();
        }

        $decoded = base64_decode($data);

        if (!static::validHMAC($decoded, $encryptionKey)) {
            return false;
        }

        $data = substr($decoded, 0, -static::HMAC_XBYTES);

        return openssl_decrypt(
            $data,
            static::CIPHER,
            hex2bin($encryptionKey),
            OPENSSL_RAW_DATA,
            base64_decode($iv)
        );
    }

    /**
     * Generate a keyed-hash message authentication code (HMAC)
     *
     * @param string $data - data to be encrypted
     * @param string $key - encryption key (hex)
     * @return string
     */
    final public static function getHMAC($data, $key)
    {
        return hash_hmac(static::HMAC_ALGO, $data, $key);
    }

    /**
     * @param $data - decrypted data with hmac
     * @param $key - encryption key (hex)
     * @return bool
     */
    final public static function validHMAC($data, $key)
    {
        $expected = static::getHMAC(substr($data, 0, -static::HMAC_XBYTES), $key);
        $hmac = substr($data, -static::HMAC_XBYTES);

        return $hmac === $expected;
    }

    /**
     * Generate a fixed-size input to a cryptographic primitive
     *
     * @return string - binary string
     */
    final public static function getInitializationVector()
    {
        return static::getRandomBytes(static::getVectorSize());
    }

    /**
     * Get size of starting value for specific cipher
     *
     * @return int
     */
    final public static function getVectorSize()
    {
        return openssl_cipher_iv_length(static::CIPHER);
    }

    /**
     * Use cryptographically secure pseudo-random number generator (CSPRNG)
     * to generate a desired length of bytes
     *
     * @return string - binary string
     */
    final public static function generateRandomKey()
    {
        return static::getRandomBytes(static::CSPRNG_BYTES);
    }

    /**
     * @param int $len - number of random bytes to generate
     * @return string - binary string
     * @throws /Exception
     */
    public static function getRandomBytes($len)
    {
        $bytes = openssl_random_pseudo_bytes($len, $strong);

        if (!$strong) {
            throw new \Exception('Algorithm not cryptographically strong');
        }

        return $bytes;
    }

    public static function getEncryptionKey()
    {
        // implement non-random key retrival here
        return bin2hex(self::generateRandomKey());
    }
}
